import { EVENTS_NAME } from '../consts';
import { Button } from '../classes/button';

export class OverlayScene extends Phaser.Scene {
  private resizeHandler!: (size: { width: number; height: number }) => void;
  private playNowBtn!: Button;
  private logoImg!: Phaser.GameObjects.Image;

  constructor() {
    super('overlay-scene');
    this.resizeHandler = (...args) => {
      this.updateChildrensPosition(...args);
    };
  }

  create(): void {
    this.initPlayNowButon();
    this.setLogo();
    this.game.scale.addListener(EVENTS_NAME.resize, this.resizeHandler);
    this.updateChildrensPosition({
      width: this.game.scale.width,
      height: this.game.scale.height,
    });
  }

  private setLogo(): void {
    this.logoImg = this.add.image(0, 0, 'spritesheet', 'logo');
  }

  private initPlayNowButon(): void {
    this.playNowBtn = new Button({
      scene: this,
      x: 0,
      y: 0,
      frame: 'btn-play-now',
      isPulsed: true,
    });

    this.playNowBtn.setInteractive();
    this.playNowBtn.on('pointerdown', () => {
      window.open('http://play.google.com/store/apps/', '_blank');
    });
  }

  private updateChildrensPosition(size: { width: number; height: number }): void {
    this.playNowBtn.setOrigin(0);
    this.playNowBtn.setPosition(
      size.width - this.playNowBtn.displayWidth - 16,
      size.height - this.playNowBtn.displayHeight - 16,
    );

    this.logoImg.setOrigin(0);
    if (this.scale.isLandscape) {
      this.logoImg.setPosition(16, 16);
    } else {
      this.logoImg.setPosition(size.width / 2 - this.logoImg.displayWidth / 2, 16);
    }
  }
}